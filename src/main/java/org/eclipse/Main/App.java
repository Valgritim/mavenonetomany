package org.eclipse.Main;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.model.Adresse;
import org.eclipse.model.Personne;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	//Persistance
    	Configuration configuration = new Configuration().configure();
    	SessionFactory sessionFactory = configuration.buildSessionFactory();
    	Session session = sessionFactory.openSession();
    	Transaction transaction = session.beginTransaction();
    	
       //Adresse
    	 	
//    	Adresse adresse = new Adresse();
//    	adresse.setRue("Lyon");
//    	adresse.setCodePostal("13015");
//    	adresse.setVille("Marseille");
//    	
//    	Adresse adresse2 = new Adresse();
//    	adresse2.setRue("Bd des écureuils");
//    	adresse2.setCodePostal("06210");
//    	adresse2.setVille("Mandelieu");
//
//    
//    	// Personne
//    	Personne personne = new Personne();
//    	personne.setPrenom("Elon");
//    	personne.setNom("Musk");
//    	personne.addAdresse(adresse);
//    	personne.addAdresse(adresse2);   	
//
//    	
//    	session.persist(personne);
//    	transaction.commit();
//    	session.close();
//    	sessionFactory.close();
    	
    	Criteria criteria = session.createCriteria(Personne.class);
    	List<Personne> personnes = criteria.list();
    	for(Personne personne : personnes) {
    	System.out.println(personne);
    	}
    	session.close();
    	sessionFactory.close();
    	
    }	
}
