package org.eclipse.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
public class Personne {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;
	private String prenom;
	private String nom;	
	
	@OneToMany(cascade= {CascadeType.PERSIST, CascadeType.REMOVE})
	//@JoinColumn(name="rue", referencedColumnName="rue", nullable=false)
	private List<Adresse> adresses = new ArrayList<Adresse>();

	public Personne(String prenom, String nom, List<Adresse> adresses) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.adresses = adresses;
	}
	public Personne() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Adresse> getAdresses() {
		return adresses;
	}
	public void setAdresses(List<Adresse> adresses) {
		this.adresses = adresses;
	}
	
	//Dans Source, choisir Generate Delegate Methods
	public boolean addAdresse(Adresse arg0) {
		return adresses.add(arg0);
	}
	public boolean removeAdresse(Adresse arg0) {
		return adresses.remove(arg0);
	}
	@Override
	public String toString() {
		return "Personne [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", adresses=" + adresses + "]";
	}	

}
